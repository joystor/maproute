<?php
/*
Plugin Name: Google Maps ROUTES v1.0
Plugin URI: http://
Description: Show POI's in a map of google maps
Version: 0.1
Author: joys
Author URI: http://
License: BSD
*/


if(!class_exists('WP_Plugin_MapRoute')){

	global $wpdb;
	$tbl_mre = $wpdb->prefix . "plugin_maproute";
	$wp_plug_mr_url = plugins_url(plugin_basename(dirname(__FILE__)));
	$wp_plug_mr_path = realpath(dirname(__FILE__));
	$wp_mre_url_gmap = '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&language=en"></script>';

	class WP_Plugin_MapRoute{
	
		public $PMR_URL = "";
		public $TBL_MRE = "";
		public $wpdb;
		
		/**
         * Construc of class
         */	
		function __construct(){
			global $tbl_mre;
			global $wpdb;
			global $wp_plug_mr_path;
			add_action('admin_menu', array(&$this, 'add_menu'));
			add_filter('the_content', array(&$this, "add_content_map") );
			add_filter('the_content', array(&$this, "add_content_pois") );
			
			$this->PMR_URL = plugins_url(plugin_basename(dirname(__FILE__)));
			$this->TBL_MRE = $tbl_mre;
			
			require_once(sprintf("%s/mapRPOIS.php", dirname(__FILE__)));
            $MRPOIS = new WP_Plugin_MapRoute_POIS();
		}
		
		/**
         * Activate plugin
         */
		public static function wp_activate(){
			global $wpdb;
			$tbl_mre = $wpdb->prefix . "plugin_maproute";
			if($wpdb->get_var("SHOW TABLES LIKE '".$tbl_mre."'") != $tbl_mre) {
				if ( ! empty( $wpdb->charset ) )
					$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
				if ( ! empty( $wpdb->collate ) )
					$charset_collate .= " COLLATE $wpdb->collate";
					
				$sql = "CREATE TABLE ".$tbl_mre." (
								id mediumint(9) NOT NULL AUTO_INCREMENT,
								datepoi TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
								idx mediumint(9) NOT NULL,
								name tinytext NOT NULL,
								description VARCHAR(500) DEFAULT '' NOT NULL,
								lat FLOAT( 10, 6 ) NOT NULL,
								lng FLOAT( 10, 6 ) NOT NULL ,
								url VARCHAR(255) DEFAULT '' NOT NULL,
								icon VARCHAR(255) DEFAULT '' NOT NULL,
								image VARCHAR(60) DEFAULT '' NOT NULL,
								UNIQUE KEY id (id)
							) $charset_collate;";
							
				require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
			}
		}

		public static function wp_deactivate(){
		}

		/**
         * Unistall plugin
         */	
		public static function wp_unistall(){
			global $wpdb;
			global $tbl_mre;
			/*$sql = "DROP TABLE ".$tbl_mre.";";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);*/
		}
		
		/**
         * add a menu
         */		
        public function add_menu(){
            // Add a page to manage this plugin's settings
        	add_plugins_page(
        	    'MapRoute POIS', 
        	    'MapRoute POIS', 
        	    'manage_options', 
        	    'map_route_configuration', 
        	    array(&$this, 'plugin_settings_page')
        	);
        }
    
        /**
         * Menu Callback
         */		
        public function plugin_settings_page(){
        	if(!current_user_can('manage_options')){
        		wp_die(__('You do not have sufficient permissions to access this page.'));
        	}
			//wp_enqueue_script
        	include(sprintf("%s/templates/adminMap.php", dirname(__FILE__)));
        }
		
		/**
         * POST Remplace Content
         * [WP_MapRoute_MAP]
		 * [WP_MR_MapH]
		 * [WP_MR_MapHW]
         */
        public function add_content_map( $content ){
        	if (strpos($content,"[WP_MapRoute_MAP]")){
				global $wp_mre_url_gmap;
				global $wp_plug_mr_url;
				
				$he = "300";
				$wi = "400";
				if (strpos($content,"[WP_MR_MapW]") && strpos($content,"[WP_MR_MapH]")){
					preg_match('/\[WP_MR_MapW\](.*?)\[WP_MR_MapW\]/', $content, $wi);
					preg_match('/\[WP_MR_MapH\](.*?)\[WP_MR_MapH\]/', $content, $he);
					if($wi[0]!=""){
						$content = preg_replace('/\[WP_MR_MapW\](.*?)\[WP_MR_MapW\]/', "", $content);
						$content = preg_replace('/\[WP_MR_MapH\](.*?)\[WP_MR_MapH\]/', "", $content);
						$he = str_replace("[WP_MR_MapH]","",$he[0]);
						$wi = str_replace("[WP_MR_MapW]","",$wi[0]);
					}
				}
				
				WP_Plugin_MapRoute::addStyle();
				WP_Plugin_MapRoute::addJs2Posts();				
				
				$dirIMG = '<input type="hidden" id="MR_dirIMGS" value="'. $wp_plug_mr_url .'"/>';
				$map = $wp_mre_url_gmap . $dirIMG . '<div id="wpmre_map-canvas" style="height:'.$he.'px;width:'.$wi.'px;"></div>';
				$content = str_replace('[WP_MapRoute_MAP]', $map, $content);
			}
			return $content;
        }
        
        /**
         * POST Remplace Content POIS
         * [WP_MapRoute_POIS]
         */
        public function add_content_pois( $content ){
        	if (strpos($content,"[WP_MapRoute_POIS]")){
				$wi = ""; //"660px";
				$stylW = "";
				$ret = preg_match('/\[WP_MapRoute_POIS\](.*?)\[WP_MapRoute_POIS\]/', $content, $wi);
				
				if($ret === 1){
					$wi = str_replace("[WP_MapRoute_POIS]","",$wi[0]);
					$stylW = 'style="width:'.$wi.'"';
				}
				
				global $wpdb;
				global $tbl_mre;
				global $wp_plug_mr_url;
				
				$sql = "select * from ".$tbl_mre." order by idx";
				$rs = $wpdb->get_results($sql, OBJECT);
				$lstPois = '<div id="list" '.$stylW.'>';
				foreach( $rs as $r ) {
					$lstPois .= '<div id="listing">';
					$lstPois .= '	<a><img src="'.$wp_plug_mr_url .'/uploads/'. $r->image.'" class="imgf"></a>';
					$lstPois .= '	<span class="number">'.$r->idx.'.</span> ';
					$lstPois .= '	<div class="info">';
					$lstPois .= '		<h2>'.$r->name.'</h2>';
					if($r->description != ""){
						$lstPois .= '		<p style="margin-bottom: 5px;">'.$r->description.'</p>';
					}else{
						$lstPois .= '<br/>';
					}
					$lstPois .= '		<a href="'.$r->url.'">View More Details</a>';
					$lstPois .= '		<br><a target="_blank" href="https://maps.google.com/maps?saddr=&amp;daddr='.$r->lat.','.$r->lng.'&amp;sensor=FALSE&amp;z=17">Get Directions</a>';
					$lstPois .= '	</div>';
					$lstPois .= '</div>';
				}
				$lstPois .= "</div>";
				WP_Plugin_MapRoute::addStyle();
				if($ret === 1){
					$content = preg_replace('/\[WP_MapRoute_POIS\](.*?)\[WP_MapRoute_POIS\]/', $lstPois, $content);
				}else{
					$content = str_replace('[WP_MapRoute_POIS]', $lstPois, $content);
				}
			}
			return $content;
        }
        
        /**
         * integrate css style file
         */
        public function addStyle(){
			global $wp_plug_mr_url;
			wp_register_style( 'MR_stylePost', $wp_plug_mr_url .'/templates/css/stylePOSTs.css' );
			wp_enqueue_style( 'MR_stylePost' );
		}
		
		/**
         * integrate js for map in posts
         */
		public function addJs2Posts(){
			wp_enqueue_script( 'my-ajax-request', plugin_dir_url( __FILE__ ) . 'templates/js/wp_mrefe.js', array( 'jquery' ) );
			wp_localize_script( 'my-ajax-request', 'MrAjx', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
		}
		
	}
}




if(class_exists('WP_Plugin_MapRoute')){
	register_activation_hook(__FILE__, array('WP_Plugin_MapRoute', 'wp_activate'));
	register_deactivation_hook(__FILE__, array('WP_Plugin_MapRoute', 'wp_deactivate'));
	register_uninstall_hook(__FILE__, array( 'WP_Plugin_MapRoute', 'wp_unistall'));

	$WP_MapRoute = new WP_Plugin_MapRoute();
	
    if(isset($WP_MapRoute)){
		
	}
}

/**
* Short description
*
* Longer more detailed description
*
* @param type $varname1 Description
* @param type $varname2 Description
* @return type Description
*/
//function boj_super_function( $varname1, $varname2 ) {
//do function stuff
//}
?>
