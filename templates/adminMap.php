<?php 
	global $wpdb;
	global $WP_MapRoute;
?>

	<link rel="stylesheet" type="text/css" href="<?php echo $WP_MapRoute->PMR_URL?>/templates/css/style.css">
	<!--link rel="stylesheet" type="text/css" href="<?php echo $WP_MapRoute->PMR_URL?>/templates/js/bootstrap-3.1.1-dist/css/bootstrap.css"-->
	
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&language=en"></script>
	<script src="<?php echo $WP_MapRoute->PMR_URL?>/templates/js/jquery-1.11.0.min.js"></script>
	<!--script src="<?php //echo $WP_MapRoute->PMR_URL?>/templates/js/bootstrap-3.1.1-dist/bootstrap.min.js"></script-->
	<script src="<?php echo $WP_MapRoute->PMR_URL?>/templates/js/jquery.form.js"></script>
	<script src="<?php echo $WP_MapRoute->PMR_URL?>/templates/js/admin.js"></script>
<div id="adminContentPOIS" style="display:none">
	<h1>POI's Administration</h1>
	<div id="panel">
		<div id="menu-options">
			<label for="add-marker">Search</label>
			<input id="autocompletePOS" /><br>
			<!--button onclick="$('#autocompletePOS').val('')">Reset</button><br/-->
			<br/>
			<label for="add-marker">Add new marker</label>
			<input id="add-marker" type="checkbox" onchange="onchangeAddMarker()"/>
			<input type="hidden" id="dirIMGS" value="<?php echo $WP_MapRoute->PMR_URL?>"/>
		</div>
		
		<div id="marker-info" style="visible:hidden; position: absolute; top: 170px;">
			<form id="newPOI">
				<input type="hidden" name="action" value="mr_addpoi"></input>
				<input type="hidden" id="newPico" name="icon" value=""></input>
				<label>Order</label><br/><input id="InOrd" name="idx" onchange="changeIcoColor(this,'newPico')">
				<br/>
				<label>Title</label><br/><input id="InTitle" name="title" onchange="mrkSetTitle(this)">
				<br/>
				<label>Description</label><br/><input id="InDesc" name="descrip" onchange="mrkSetInfo(this)">
				<br/>
				<label>Link to</label><br/>
				<select id="selPosts" name="purl">
				<?php 
					$querystr = "
						SELECT $wpdb->posts.post_title, $wpdb->posts.guid, $wpdb->posts.id
						FROM $wpdb->posts
						WHERE $wpdb->posts.post_status = 'publish' 
						AND 
						  ($wpdb->posts.post_type = 'post' OR $wpdb->posts.post_type = 'page' )
						ORDER BY $wpdb->posts.post_date DESC
					 ";
					$rs = $wpdb->get_results($querystr, OBJECT);
					foreach( $rs as $r ) {
                        $urlPerm = get_permalink( $r->id );
						echo "<option value='".$urlPerm."'>".$r->post_title."</option>";
					}
				?>
				</select>
				
				<br/>
				<label>Upload a file</label><br/><input type="file" name="image"></input>
				<br/>

				<label>latitude</label><br/><input id="InLat" name="lat"></input>
				<br/>
				<label>longitude</label><br/><input id="InLng" name="lng"></input>
				<br/>

				<input id="latlong" style="display:none"/>
				<label>Click on map</label><br/>
				<input id="chooseLatLog" name="coordx" readonly style="display:none"/>
				<br/>
				<button type="send">Add new POI</button>
			</form>
		</div>
		
		
		<div id="marker-info-upd" style="visible:hidden; position: absolute; top: 170px;">
			<form id="updPOI">
				<input type="hidden" id="updPico" name="icon" value=""></input>
				<input id="InModID" type="hidden" name="id"></input>
				<br/>
				<label>Order</label><br/><input id="InModOrd" name="idx" onchange="changeIcoColor(this,'updPico')">
				<br/>
				<label>Title</label><br/><input id="InModTitle" name="title" onchange="mrkSetTitle(this)">
				<br/>
				<label>Description</label><br/><input id="InModDesc" name="descrip" onchange="mrkSetInfo(this)">
				<br/>
				<label>Link to</label><br/>
				<select id="selModPosts" name="purl">
				<?php 
					$querystr = "
						SELECT $wpdb->posts.post_title, $wpdb->posts.guid
						FROM $wpdb->posts
						WHERE $wpdb->posts.post_status = 'publish' 
						AND $wpdb->posts.post_type = 'post'
						ORDER BY $wpdb->posts.post_date DESC
					 ";
					$rs = $wpdb->get_results($querystr, OBJECT);
					foreach( $rs as $r ) {
						echo "<option value='".$r->guid."'>".$r->post_title."</option>";
					}
				?>
				</select>
				<br/>
				<label>Upload a file</label><br/><input type="file" name="image"></input>
				<br/>

				<label>latitude</label><br/><input id="InModLat" name="lat"></input>
				<br/>
				<label>longitude</label><br/><input id="InModLng" name="lng"></input>
				<br/>

				<input id="InUpdAction" type="hidden" name="action"></input>
				<button type="send" onclick="$('#InUpdAction').val('mr_updpoi')">Update POI</button>
				<button type="send" onclick="$('#InUpdAction').val('mr_delpoi')">Delete</button>
			</form>
		</div>
		
	</div>
	<div id="map-canvas"></div>

	<!--button type="button" class="btn btn-default btn-lg" style="position:absolute;top: 10px; right: 10px;">
		<span class="glyphicon glyphicon-list-alt"></span> POI's Table
	</button-->
	<div id="mrDivContTable" style="display:none;">
		<a class="boxclose" id="boxclose">X</a>
		<div style="bottom: 1px;position: absolute;top: 30px;left: 2px;right: 2px;overflow: auto;">
			<div id="mrDivTabCont" class="CSSTableGenerator"></div>
		</div>
	</div>
	
	<div id="mrDivContConf" style="display:none;width:300px;height:200px">
		<a class="boxclose" id="boxclose2">X</a>
		<div style="bottom: 1px;position: absolute;top: 30px;left: 2px;right: 2px;overflow: auto;">
			<div id="mrDivConfCont">
				<form id="frmSendCFGMR" enctype="multipart/form-data">
					<input id="chkUseNumb" type="checkbox" name="useNumb" value="true" checked><label for="chkUseNumb">Use Numbers</label><br>
					<input id="chkUseKMLP" type="checkbox" name="useKMLRoute" value="true"><label for="chkUseKMLP"><span id="KMLMSG"> Use KML Route</span></label><br>
					<input type="hidden" name="action" value="mr_setcfg"></input>
					<div id="shwFileKML" style="display:none">
						<input type="file" name="fKML" accept=".kml,.kmz"></input>
					</div>
					<br/>
					<button type="send" id="btnSaveCFG">Save</button>
				</form>
			</div>
		</div>
	</div>
	
	<input type="submit" id="mrBtnOpnTable" class="button" value="POI's Table" style="position:absolute;top: 10px; right: 10px;">
	<input type="submit" id="mrBtnOpnCfg" class="button" value="Options" style="position:absolute;top: 10px; right: 120px;">
</div>
