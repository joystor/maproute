var map;
var lRoute;
var mrkNew;
var currMark;
var iconBase = 'images/map-marker_';
var lastClick;
var mrkInfoWindow;
var allMarks = [];
var autocompletePOS;
var icoA = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=";
var icoB = "|709fea|000000";

var maxPOI = 0;


/*#############################################################################################*/
function init() {
	icoA = $("#dirIMGS").val() + "/templates/images/number_";
	icoB = ".png";
    document.getElementById("marker-info").style.visibility = 'hidden';
    document.getElementById("marker-info-upd").style.visibility = 'hidden';
    document.getElementById("add-marker").checked = false;
    document.getElementById('chooseLatLog').value = "";
    
    cleanData();
    
    if(google === undefined){
        return false;
    }

    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: new google.maps.LatLng(39.3025, -97.5585),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var panelDiv = document.getElementById('panel');

    var data = new PlacesDataSource(map);    

    mrkNew = addMarker( 
		new google.maps.LatLng(51.450584, -0.282898), 
		"green","TITLE","content");
	mrkNew.setMap(null);
	
	var urlPin = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=?|709fea|000000";
	var pin = new google.maps.MarkerImage(
		urlPin,
		null, null, null,
		null //new google.maps.Size(32, 32)
	);
	mrkNew.setIcon(pin);
	

    google.maps.event.addListener(map, 'click', function (event) {
        if (document.getElementById("add-marker").checked) {
            document.getElementById('chooseLatLog').value = document.getElementById('latlong').value;
            mrkNew.setPosition( lastClick );
            mrkNew.setMap(map);
            $("#InLat").val( mrkNew.getPosition().lat() );
			$("#InLng").val( mrkNew.getPosition().lng() );
        }
    });

    google.maps.event.addListener(map, 'mousemove', function (event) {
        document.getElementById('latlong').value = event.latLng.lat() + ', ' + event.latLng.lng();
        lastClick = new google.maps.LatLng( event.latLng.lat(), event.latLng.lng() );
    });
	
	
	autocompletePOS = new google.maps.places.Autocomplete(
	/** @type {HTMLInputElement} */(document.getElementById('autocompletePOS')),
	{
		types: ['(regions)']
		/*componentRestrictions:  { 'country': 'uk' }*/
	});
	google.maps.event.addListener(autocompletePOS, 'place_changed', onPlaceChanged);

	getAllMrkSaveds();
}
/*#############################################################################################*/
/*#############################################################################################*/
function cleanData(){
	$(':input','#updPOI')
		  .not(':button, :submit, :reset, :hidden')
		  .val('')
		  .removeAttr('checked')
		  .removeAttr('selected');
	$(':input','#newPOI')
		  .not(':button, :submit, :reset, :hidden')
		  .val('')
		  .removeAttr('checked')
		  .removeAttr('selected');
}
/*#############################################################################################*/
/*#############################################################################################*/
function getAllMrkSaveds(){
	clearAllMarks();
	$.post(ajaxurl, {"action" : "mr_getpois"},function(json){
			$(json.rows).each(function(i,v){
				var pin = new google.maps.MarkerImage(
					v.icon, null, null, null, null ); 				
				var contentString = '<div id="content">'+
					  '<div id="siteNotice">'+
					  '</div>'+
					  '<img src="'+$("#dirIMGS").val()+'/uploads/'+v.image+'" alt="" '+
					  ' style="width:100px;left:0px;position: absolute;">' +
					  '<div id="bodyContent" '+
					  'style="left:110px;position: absolute;">' +
					  '<strong>'+v.idx+' .- '+v.name+'</strong>' +
					  '<br/>'+
					  v.description  +
					  '</div>'+
					  '</div>';
				var InfoW = new google.maps.InfoWindow({
					content: contentString,
					maxWidth: 320
				});
				
				var marker = new google.maps.Marker({
					title : v.name,
					position: new google.maps.LatLng(parseFloat(v.lat), parseFloat(v.lng)),
					icon: pin,
					map: map,
					draggable:true,
					animation: google.maps.Animation.DROP,
					iddb : v.id,
					idx : v.idx,
					information : v.description,
					infoPop : InfoW
				});
				
				google.maps.event.addListener(marker, 'click', function () {
					clearAllPopUps();
					//InfoW.open(map, marker);
					document.getElementById("marker-info").style.visibility = 'hidden';
					document.getElementById("marker-info-upd").style.visibility = 'visible';
					$("#InModID").val(v.id);
					$("#InModOrd").val(v.idx);
					$("#InModTitle").val(v.name);
					$("#InModDesc").val(v.description);
					$("#selModPosts").val(v.url);
					$("#InLat").val( marker.getPosition().lat() );
					$("#InLng").val( marker.getPosition().lng() );
					
					$("#InModLat").val( marker.getPosition().lat() );
					$("#InModLng").val( marker.getPosition().lng() );
					currMark = marker;
				});
				google.maps.event.addListener(marker, 'dragend', function() {
					//console.log("id: "+marker.iddb);
					$("#InModLat").val( marker.getPosition().lat() );
					$("#InModLng").val( marker.getPosition().lng() );
					$.post(ajaxurl,{
						"action":"mr_movpoi",
						"lat":marker.getPosition().lat(),
						"lng":marker.getPosition().lng(),
						"id":marker.iddb
						},function(json){
						});
					currMark = marker;
				});
				allMarks.push(marker);
			});
		},"json");
		
	$.post(ajaxurl, {"action" : "mr_getcfg"},function(json){
		if(json.hasOwnProperty("useKMLRoute")){
			if(json.useKMLRoute=="true"){
				lRoute = new google.maps.KmlLayer({
					  url: $("#dirIMGS").val()+'/uploads/'+json.pathKML,
					  suppressInfoWindows: true
				  });
				lRoute.setMap(map);
			}else{
				if(lRoute != undefined){
					lRoute.setMap(null);
				}
			}
		}
	});
}


/*#############################################################################################*/
function onPlaceChanged() {
	var place = autocompletePOS.getPlace();
	if (place.geometry) {
		map.panTo(place.geometry.location);
		map.setZoom(14);
	} else {
		document.getElementById('autocompletePOS').placeholder = 'Where are you?';
	}
}
/*#############################################################################################*/
function addMarker(pos, color, title, msg) {
	var pin = new google.maps.MarkerImage(
		iconBase + color +".png",
		null, null, null,
		new google.maps.Size(32, 32)
	); 
	
    var marker = new google.maps.Marker({
		title : title,
        position: pos,
        icon: pin,
        map: map
    });
    
    var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading">'+
      document.getElementById("InTitle").value+
      '</h3>'+
      '<div id="bodyContent">'+"INFO"+
      '</div>'+
      '</div>';

    mrkInfoWindow = new google.maps.InfoWindow({
		content: contentString
	});

    google.maps.event.addListener(marker, 'click', function () {
        mrkInfoWindow.open(map,marker);
    });
    return marker;
}
/*#############################################################################################*/
function onchangeAddMarker() {
    if (document.getElementById("add-marker").checked) {
		document.getElementById('chooseLatLog').value = "";    
		document.getElementById('InTitle').value = "";
		/*if(document.getElementById('InOrd').value==""){
			document.getElementById('InOrd').value = "0";
		}*/
		$.each(allMarks, function(i,v){
			if(parseInt(v.idx)>maxPOI){
				maxPOI = parseInt(v.idx);
			}
		});
		document.getElementById('InOrd').value = maxPOI +1
        document.getElementById("marker-info").style.visibility = 'visible';
		$("#marker-info").css("top", $("#add-marker").position().top+30 +"px");
    } else {
        document.getElementById("marker-info").style.visibility = 'hidden';
    }
    document.getElementById("marker-info-upd").style.visibility = 'hidden';
}


/*#############################################################################################*/
function changeIcoColor(obj,inp){
	var url = icoA + parseInt($(obj).val()) + icoB;
	$("#"+inp).val(url);
	var pin = new google.maps.MarkerImage(
		url,
		null, null, null,
		null //new google.maps.Size(32, 32)
	);
	mrkNew.setIcon(pin);
	mrkSetInfo();
}

function mrkSetTitle(obj){
	mrkNew.setTitle( $(obj).val() );
	mrkSetInfo();
}

function mrkSetInfo(obj){
	var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading">'+
      document.getElementById("InTitle").value+
      '</h3>'+
      '<div id="bodyContent">'+
      $(obj).val() +
      '</div>'+
      '</div>';
	mrkInfoWindow = new google.maps.InfoWindow({
		content: contentString
	});
}



/*#############################################################################################*/


/**
 * Creates a new PlacesDataSource.
 * @param {google.maps.Map} map
 * @constructor
 */
function PlacesDataSource(map) {
    this.service_ = new google.maps.places.PlacesService(map);
}

/**
 * @inheritDoc
 */
PlacesDataSource.prototype.getStores = function (bounds, features, callback) {
    this.service_.search({
        bounds: bounds
    }, function (results, status) {
        var stores = [];

        for (var i = 0, result; result = results[i]; i++) {
            var latLng = result.geometry.location;
            var store = new storeLocator.Store(result.id, latLng, null, {
                title: result.name,
                address: result.types.join(', '),
                icon: result.icon
            });
            stores.push(store);
        }

        callback(stores);
    });
};



window.onload = init;




/*#############################################################################################*/
function clearAllMarks() {
  for (var i = 0; i < allMarks.length; i++ ) {
    allMarks[i].setMap(null);
  }
  allMarks.length = 0;
}

function clearAllPopUps() {
  for (var i = 0; i < allMarks.length; i++ ) {
    allMarks[i].infoPop.close();
  }
}




/*#############################################################################################*/
$().ready(function(){

	$("#adminContentPOIS").fadeIn();
	
	$("#InOrd").on("change",function(){
		$("#InOrd").val( parseInt(  $("#InOrd").val() ) );
	});
	$("#InModOrd").on("change",function(){
		$("#InModOrd").val( parseInt(  $("#InModOrd").val() ) );
	});
	
	$("#newPOI").submit(function(e){
		var formData = new FormData(this);
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			success: function(data, textStatus, jqXHR){
				document.getElementById("marker-info").style.visibility = 'hidden';
				cleanData();
				mrkNew.setMap(null);
				onchangeAddMarker();
				clearAllMarks();
				getAllMrkSaveds();
			},
			error: function(jqXHR, textStatus, errorThrown){
			}         
		});
		e.preventDefault();
		e.unbind();
	});
	
	$("#updPOI").submit(function(e){
		
		var formData = new FormData(this);
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			success: function(data, textStatus, jqXHR){
				//console.log(jqXHR);
				cleanData();
				document.getElementById("marker-info-upd").style.visibility = 'hidden';
				clearAllMarks();
				getAllMrkSaveds();
			},
			error: function(jqXHR, textStatus, errorThrown){
			}         
		});
		e.preventDefault();
		e.unbind();
	}); 
	
	$("#mrBtnOpnTable").on("click",function(){
		 $("#mrBtnOpnTable").attr("disabled", "disabled");
		if(!$("#mrDivContTable").is(":visible")){
			$.post(ajaxurl, {"action" : "mr_getpois"},function(json){
				mrCreatTab(json);
				$("#mrDivContTable").toggle();
				$("#mrBtnOpnTable").removeAttr("disabled");      
			});
		}else{
			$("#mrDivContTable").toggle();
			$("#mrBtnOpnTable").removeAttr("disabled");      
		}
	});
	
	$("#mrBtnOpnCfg").on("click",function(){
		 $("#mrBtnOpnCfg").attr("disabled", "disabled");
		if(!$("#mrDivContConf").is(":visible")){
			$.post(ajaxurl, {"action" : "mr_getcfg"},function(json){
				console.log(json);
				if(json.useNumbers=="true"){
					$("#chkUseNumb").prop("checked",true);
				}else{
					$("#chkUseNumb").prop("checked",false);
				}
				if(!json.hasOwnProperty("useKMLRoute")){
					$("#chkUseKMLP").prop("checked",false);
				}
				if(json.useKMLRoute=="true"){
					$("#chkUseKMLP").prop("checked",true);
					$("#KMLMSG").html("Use KML Route ("+json.pathKML+")");
					$("#shwFileKML").fadeIn();
				}else{
					$("#chkUseKMLP").prop("checked",false);
					$("#KMLMSG").html("Use KML Route");
					$("#shwFileKML").fadeOut();
				}
				$("#mrDivContConf").toggle();
				$("#mrBtnOpnCfg").removeAttr("disabled"); 
			});
		}else{
			$("#mrDivContConf").toggle();
			$("#mrBtnOpnCfg").removeAttr("disabled"); 
		}
	});
	
	$('#chkUseKMLP').change(function() {
		var $checkbox = $(this);
		if ($checkbox.prop('checked')) {
			$("#shwFileKML").fadeIn();
		} else {
			$("#shwFileKML").fadeOut();
		}
	});
	
	$("#btnSaveCFG").on("click",function(){
		$("#btnSaveCFG").attr("disabled", "disabled");
		$("#frmSendCFGMR").submit();
		/*$.post(ajaxurl,{
				"action" : "mr_setcfg",
				"useNumb": $("#chkUseNumb").is(":checked")?"true":"false",
				"useKMLRoute": $("#chkUseKMLP").is(":checked")?"true":"false"
			},
			function(json){
				getAllMrkSaveds();
				$("#mrDivContConf").hide();
			});
		*/
	});
	
	/*$("#frmSendCFGMR").submit( function( e ) {
		$.ajax( {
			url: ajaxurl,
			type: 'POST',
			data: new FormData( this ),
			processData: false,
			contentType: false
		}).done(function() {
			$("#mrDivContConf").toggle();
			getAllMrkSaveds();
		});
		e.preventDefault();
	});*/
	
	$("#frmSendCFGMR").ajaxForm({
		url: ajaxurl,
		type: 'POST',
		success: function(responseText, statusText, xhr, $form){
			clearAllMarks();
			$("#mrDivContConf").hide();
			$("#btnSaveCFG").removeAttr("disabled");   
			getAllMrkSaveds();
		}
	}); 
	
	
	
	
	$("#boxclose").on("click",function(){
		$("#mrDivContTable").hide();
		getAllMrkSaveds();
	});
	$("#boxclose2").on("click",function(){
		$("#mrDivContConf").hide();
	});

	$("#InLat").on("change", function(){
		if(isFinite(String($("#InLat").val()).trim() || NaN) &&
		   isFinite(String($("#InLng").val()).trim() || NaN)){
		   	var pos = new google.maps.LatLng( parseFloat ($("#InLat").val()), parseFloat ($("#InLng").val()) );
			mrkNew.setPosition( pos );
            mrkNew.setMap(map);
            document.getElementById('chooseLatLog').value = mrkNew.getPosition().lat() + ', ' + mrkNew.getPosition().lng();
		}
	});
	$("#InLng").on("change",function(){
		if(isFinite(String($("#InLat").val()).trim() || NaN) &&
		   isFinite(String($("#InLng").val()).trim() || NaN)){
			var pos = new google.maps.LatLng( parseFloat ($("#InLat").val()), parseFloat ($("#InLng").val()) );
			mrkNew.setPosition( pos );
            mrkNew.setMap(map);
            document.getElementById('chooseLatLog').value = mrkNew.getPosition().lat() + ', ' + mrkNew.getPosition().lng();
		}
	});
	
	
	$("#InModLat").on("change", function(){
		if(isFinite(String($("#InModLat").val()).trim() || NaN) &&
		   isFinite(String($("#InModLng").val()).trim() || NaN)){
		   	var pos = new google.maps.LatLng( parseFloat ($("#InModLat").val()), parseFloat ($("#InModLng").val()) );
			currMark.setPosition( pos );
			var url = icoA + $("#InModOrd").val() + icoB;
			$("#updPico").val(url);
		}
	});
	$("#InModLng").on("change",function(){
		if(isFinite(String($("#InModLat").val()).trim() || NaN) &&
		   isFinite(String($("#InModLng").val()).trim() || NaN)){
			var pos = new google.maps.LatLng( parseFloat ($("#InModLat").val()), parseFloat ($("#InModLng").val()) );
			currMark.setPosition( pos );
			var url = icoA + $("#InModOrd").val() + icoB;
			$("#updPico").val(url);
		}
	});
	
	
});


function mrCreatTab(json){
	var html = "<div class='Table'>";
	html+= "<div class='Heading' style='background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);'>";
	html+= "<div class='Cell'><p>Order</p></div>";
	html+= "<div class='Cell'><p>Title</p></div>";
	html+= "<div class='Cell'><p>Description</p></div>";
	html+= "<div class='Cell'><p>Image</p></div>";
	html+= "<div class='Cell'><p>URL</p></div>";
	html+= "<div class='Cell'><p>Latitude</p></div>";
	html+= "<div class='Cell'><p>Longitude</p></div>";
	html+= "<div class='Cell'><p>Save</p></div>";
    html+= "<div class='Cell'><p>Delete</p></div>";
	html+= "</div>";
	var tOdd = true;
	var style = "";
	$(json.rows).each(function(i,v){
		if(tOdd){
			style = 'background-color:#e5e5e5';
		}else{
			style = 'background-color:#ffffff';
		}
		tOdd = !tOdd;
		html+= "<div class='Row' style='"+style+"'>";
		html+= "<div class='Cell'><p><input id='idx"+v.id+"' value='"+v.idx+"' style='width:30px;'/></p></div>";
		html+= "<div class='Cell'><p><input id='tit"+v.id+"' value='"+v.name+"'/></p></div>";
		html+= "<div class='Cell'><p><input id='des"+v.id+"' value='"+v.description+"'/></p></div>";
		html+= "<div class='Cell'><p><input id='imgNam"+v.id+"' value='"+v.image+"'/>"+
				"<form id='frm"+v.id+"' enctype='multipart/form-data'>"
				+"<input type='hidden' name='action' value='mr_upFi'/>"
				+"<input id='img"+v.id+"' type='file' name='fIMG'></input></form>"
				+"</p></div>";
		html+= "<div class='Cell'><p><input id='url"+v.id+"' value='"+v.url+"'/></p></div>";
		html+= "<div class='Cell'><p><input id='lat"+v.id+"' value='"+v.lat+"' style='width:90px;'/></p></div>";
		html+= "<div class='Cell'><p><input id='lng"+v.id+"' value='"+v.lng+"' style='width:90px;'/></p></div>";
        html+= "<div class='Cell'><p><button onclick='mrSavePOItbl("+v.id+")' style='width:50px;'>Save</button></p></div>";
		html+= "<div class='Cell'><p><a href='#' onclick='mrDelPOItbl("+v.idx+","+v.id+")' style='width:25px;'>X</a></p></div>";
		html+= "</div>";
	});
	html+= "</div>";
	$("#mrDivTabCont").html( html );
}

function mrDelPOItbl(idx, id){
	$.post(ajaxurl, {"action" : "mr_delpoi","idx":idx,"id":id},function(j){
		$.post(ajaxurl, {"action" : "mr_getpois"},function(json){
				mrCreatTab(json);
		});
	});
}

function mrSavePOItbl(id){

	$("#frm"+id).ajaxForm({
		url: ajaxurl,
		type: 'POST',
		success: function(responseText, statusText, xhr, $form){
			console.log(statusText);
		}
	}).submit();
	
    if($("#img"+id).val() != ""){
        $("#imgNam"+id).val( $("#img"+id).val().replace("C:\\fakepath\\","") );
    }
	
    $.post(ajaxurl, 
            {
                "action" : "mr_updpoi",
                "id":id,
                "idx":$("#idx"+id).val(),
                "title":$("#tit"+id).val(),
                "purl":$("#url"+id).val(),
                "descrip":$("#des"+id).val(),
                "lat":$("#lat"+id).val(),
                "lng":$("#lng"+id).val(),
				"imgName": $("#img"+id).val().replace("C:\\fakepath\\","")
            },function(j){
            $.post(ajaxurl, {"action" : "mr_getpois"},function(json){
				mrCreatTab(json);
            });
	});
}
