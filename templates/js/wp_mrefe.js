var mre_map;
var mre_lRoute;
var allMarks = [];
var gDir;

function init() {
	mre_map = new google.maps.Map(document.getElementById('wpmre_map-canvas'), {
        center: new google.maps.LatLng(39.3025, -97.5585),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    gDir = new google.maps.DirectionsService();
    getAllMrkSaveds();
}

window.onload = init;





function getAllMrkSaveds(){
	clearAllMarks();
	jQuery.post(MrAjx.url, {"action" : "mr_getpois"},function(json){
			jQuery(json.rows).each(function(i,v){
				var pin = new google.maps.MarkerImage(
					v.icon, null, null, null, null );
				var desc = "";
				if(v.description != ""){
					desc = "<br/><span>"+v.description+"</span>";
				}
				var contentString = '<div class="infoWindow">'+
					  '<div id="siteNotice">'+
					  '</div>'+
					  '<img src="'+jQuery("#MR_dirIMGS").val()+'/uploads/'+v.image+'" alt="" '+
					  ' style="width:100px;left:0px;position: absolute;">' +
					  '<div id="bodyContent" '+
					  'style="left:110px;position: absolute;">' +
					  '<strong>'+v.idx+' .- '+v.name+'</strong>' +
					  desc +
					  '<br/>'+
					  v.lat+","+v.lng  +
					  '<br><a href="'+v.url+'">View More Details</a>' +
					  '<br><a target="_blank" href="https://maps.google.com/maps?saddr=&amp;daddr='+v.lat+','+v.lng+'&amp;sensor=FALSE&amp;z=17">Get Directions</a>' +
					  '</div>'+
					  '</div>';
				var InfoW = new google.maps.InfoWindow({
					content: contentString,
					maxWidth: 420
					/*minWidth: 220,
					padding: 7,
					minHeight: 90*/
				});
				
				var marker = new google.maps.Marker({
					title : v.name,
					position: new google.maps.LatLng(parseFloat(v.lat), parseFloat(v.lng)),
					icon: pin,
					map: mre_map,
					draggable:false,
					animation: google.maps.Animation.DROP,
					iddb : v.id,
					idx : v.idx,
					information : v.description,
					infoPop : InfoW
				});
				
				if(i==0){
					mre_map.setCenter(marker.getPosition());
					mre_map.setZoom(12);
				}
				
				google.maps.event.addListener(marker, 'click', function () {
					clearAllPopUps();
					InfoW.open(mre_map, marker);
				});
				allMarks.push(marker);
			});
			makeWays();
		},"json");
}

function makeWays(){
	jQuery.post(MrAjx.url, {"action" : "mr_getcfg"},function(json){
		if(json.hasOwnProperty("useKMLRoute")){
			if(json.useKMLRoute=="true"){
				mre_lRoute = new google.maps.KmlLayer({
					  url: jQuery("#MR_dirIMGS").val()+'/uploads/'+json.pathKML,
					  suppressInfoWindows: true
				  });
				mre_lRoute.setMap(mre_map);
			}else{
				if(mre_lRoute != undefined){
					mre_lRoute.setMap(null);
				}
				makeRouteGM();
			}
		}else{
			makeRouteGM();
		}
	});

	
}


function makeRouteGM(){
	var step = false;
	var start = null;
	var ends = null;
	var waypts = [];
	var wayI = 0;
	var coord;
	for (var i = 0; i < allMarks.length; i++) {
		var mrk = allMarks[i];
		coord = mrk.getPosition();
		if(step){
			waypts.push({
				  location:coord,
				  stopover:true
			});
			wayI++;
			if(wayI == 8){
				ends = coord;
				setDirection(start, ends, waypts);
				start = ends;
				wayI = 0;
				waypts = [];
			}
		}else{
			start = coord;
			step=!step;
		}		
	}
	if(waypts.length > 0 && start != undefined ){
		ends = coord;
		setDirection(start, ends, waypts);
	}
}


function setDirection(start, ends, waypts){
	var polyline = new google.maps.Polyline({
					path: [],
					strokeColor: '#0000FF',
					strokeOpacity: 0.8,
					strokeWeight: 3
				});
	var req1 = {
		  origin: start,
		  destination: ends,
		  waypoints: waypts,
		  optimizeWaypoints: true,
		  travelMode: google.maps.TravelMode.DRIVING
	  };
	setTimeout(
		function(){
			gDir.route(req1, function(response, status) {
			  if (status == google.maps.DirectionsStatus.OK) {
					path = response.routes[0].overview_path;
					jQuery(path).each(function(index, item) {
						polyline.getPath().push(item);
					});
					polyline.setMap(mre_map);
					zoomToObject(polyline);
			  }else{
				console.log(status);
			  }
			});
		},
		300
	);
}


function clearAllMarks() {
  for (var i = 0; i < allMarks.length; i++ ) {
    allMarks[i].setMap(null);
  }
  allMarks.length = 0;
}

function clearAllPopUps() {
  for (var i = 0; i < allMarks.length; i++ ) {
    allMarks[i].infoPop.close();
  }
}

function zoomToObject(obj){
    var bounds = new google.maps.LatLngBounds();
    var points = obj.getPath().getArray();
    for (var n = 0; n < points.length ; n++){
        bounds.extend(points[n]);
    }
    mre_map.fitBounds(bounds);
}
