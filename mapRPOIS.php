<?php

if(!class_exists('WP_Plugin_MapRoute_POIS')){
	class WP_Plugin_MapRoute_POIS{
		
		function __construct(){
			add_action('wp_ajax_mr_getpois', array(&$this, 'getPOIS'));
            add_action('wp_ajax_mr_getpoisGrid', array(&$this, 'getPOISGrid'));
			add_action('wp_ajax_nopriv_mr_getpois', array(&$this, 'getPOIS'));
            add_action('wp_ajax_nopriv_mr_getpoisGrid', array(&$this, 'getPOISGrid'));
			add_action('wp_ajax_mr_addpoi', array(&$this, 'addPOI'));
			add_action('wp_ajax_mr_updpoi', array(&$this, 'updPOI'));
			add_action('wp_ajax_mr_movpoi', array(&$this, 'movPOI'));
			add_action('wp_ajax_mr_delpoi', array(&$this, 'delPOI'));
			
			add_action('wp_ajax_mr_getcfg', array(&$this, 'getCFG'));
			add_action('wp_ajax_nopriv_mr_getcfg', array(&$this, 'getCFG'));
			add_action('wp_ajax_mr_setcfg', array(&$this, 'setCFG'));
			
			add_action('wp_ajax_mr_upFi', array(&$this, 'upFile'));
		}
		
		function getPOIS(){
			global $wpdb;
			global $tbl_mre;
			
			global $wp_plug_mr_path;
			$file = $wp_plug_mr_path. '/mrcfg.obj';
			$useNumbers = false;
			if( file_exists( $file )) {
				$cfgFile = file_get_contents( $file );
				$cfg = unserialize($cfgFile);
				if( $cfg['useNumbers'] == "true"){
					$useNumbers = true;
				}
			}
			
			$sql = "select * from ".$tbl_mre . " order by idx";
			$rows = array();
			$rs = $wpdb->get_results($sql, OBJECT);
			foreach( $rs as $r ) {
				if( !$useNumbers ){
					//$r->icon = preg_replace('/number_(.*?).png/', "number_.png", $r->icon);
                    $r->icon = plugins_url(plugin_basename(dirname(__FILE__))) . "/templates/images/number_.png";
				}else{
                    $r->icon = plugins_url(plugin_basename(dirname(__FILE__))) . "/templates/images/number_". $r->idx . ".png";
                }
				$rows[] = $r;
			}
			$data = array('rows' => $rows);
			header( 'Content-Type: application/json' );
			echo json_encode($data);
			die();
		}
        
        function getPOISGrid(){
			global $wpdb;
			global $tbl_mre;
			$sql = "select * from ".$tbl_mre . " order by idx";
			$rows = array();
			$rs = $wpdb->get_results($sql, OBJECT);
			foreach( $rs as $r ) {
                $row = array(
                            "id"=>"$r->id",
                            "cell"=>$r
                            );
				//$rows[] = $row;
                array_push($rows, $row);
			}
            
			$data = array('rows' => $rows);
			header( 'Content-Type: application/json' );
			echo json_encode($data);
			die();
		}
		
		function addPOI(){
			global $wpdb;
			global $tbl_mre;
			global $wp_plug_mr_path;
			
			$idx = $_POST['idx'];
			$tit = $_POST['title'];
			$purl = $_POST['purl'];
			$coor = $_POST['coordx'];
			$descrip = $_POST['descrip'];
			$icon = $_POST['icon'];
			$c = explode(",", $coor);
			$fil = $_FILES["image"]["name"];
			
			move_uploaded_file($_FILES["image"]["tmp_name"],
				   $wp_plug_mr_path. "/uploads/" . $_FILES["image"]["name"]);
				   
				   
			$sql = "select max(id) as id from ".$tbl_mre . "";
			$rs = $wpdb->get_results($sql, OBJECT);
			$maxID = 0;
			foreach( $rs as $r ) {
				$maxID = $r->id;
			}
				   
			$rs_affected = $wpdb->insert( $tbl_mre, 
					array( 
					    'id' => $maxID +1,
					    'idx' => $idx, 
						'name' => $tit, 
						'description' => $descrip,
						'url' => $purl,
						'lat' => $c[0],
						'lng' => $c[1],
						'icon' => $icon,
						'image' =>  $fil) );
			
			$data = array('rows' => $rs_affected, "img" => $wp_plug_mr_path. "/uploads/" . $_FILES["image"]["name"]);
			header( 'Content-Type: application/json' );
			echo json_encode($data);
			die();
		}
		
		function updPOI(){
			global $wpdb;
			global $tbl_mre;
			global $wp_plug_mr_path;
			$id = $_POST['id'];
			$idx = $_POST['idx'];
			$tit = $_POST['title'];
			$purl = $_POST['purl'];
			$descrip = $_POST['descrip'];
            
            $icon = "";
            if(isset($_POST['icon']) && $_POST['icon']!=""){
                $icon = $_POST['icon'];
                $icon = ", icon='$icon' ";
            }
			
            $coords = "";
			$c = array();
			if(isset($_POST['coordx'])){
				$coor = $_POST['coordx'];
				$c = explode(",", $coor);
                $coords = ", lat = ".$c[0] .
                          ", lng = ".$c[1];
			}else if(isset($_POST['lat']) && isset($_POST['lng'])){
                $coords = ", lat = ".$_POST['lat'] .
                          ", lng = ".$_POST['lng'];
			}
			
			$img ="";
			if( is_uploaded_file($_FILES['image']['tmp_name']) ) {
				if(isset($_FILES["image"]['name']) && $_FILES["image"]['name']!=''){
					move_uploaded_file($_FILES["image"]["tmp_name"],
						$wp_plug_mr_path. "/uploads/" . $_FILES["image"]["name"]);
					$img =",image='".$_FILES["image"]["name"]."'";
				}
			}
			
			if(isset($_POST['imgName'])){
                if( $_POST['imgName'] != "" ){
                    $img = ", image='".$_POST['imgName']."'";
                }
			}
			
			$sql = "update ".$tbl_mre." set idx=$idx, " .
					" name='$tit', description='$descrip', url='$purl' " .
					" $coords " .
					" $img " .
					" $icon " .
					" where id=".$id;
			$rs = $wpdb->get_results($sql, OBJECT);
			$data = array('rows' => "1", "sql"=> "$sql");
			header( 'Content-Type: application/json' );
			echo json_encode($data);
			die();
		}
		
		function movPOI(){
			global $wpdb;
			global $tbl_mre;
			
			$id = $_POST['id'];
			$lat = $_POST['lat'];
			$lng = $_POST['lng'];
			
			$sql = "update ".$tbl_mre." set lat=$lat, lng=$lng where id=".$id;
			$rs = $wpdb->get_results($sql, OBJECT);
			$data = array('rows' => "1");
			header( 'Content-Type: application/json' );
			echo json_encode($data);
			die();
		}
		
		
		function delPOI(){
			global $wpdb;
			global $tbl_mre;
			
			$idx = $_POST['idx'];
			$id = $_POST['id'];
			
			$sql = "delete from ".$tbl_mre." where id=".$id;
			$rs = $wpdb->get_results($sql, OBJECT);
			$data = array('rows' => "1");
			header( 'Content-Type: application/json' );
			echo json_encode($data);
			die();
		}
		
		function getCFG(){
			global $wp_plug_mr_path;
			
			$file = $wp_plug_mr_path. '/mrcfg.obj';
			if (!file_exists( $file )) {
				// set and write data for example
				$cfg = array(
						'useNumbers' => true,
						'useKMLRoute' => false,
						'pathKML' => ""
					);
				$fp = fopen( $file ,'w');
				fwrite($fp,serialize($cfg));
			}
			
			// Reading the data
			$cfgFile = file_get_contents( $file );
			$cfg = unserialize($cfgFile);
			header( 'Content-Type: application/json' );
			echo json_encode($cfg);
			die();
		}
		
		function setCFG(){
			global $wp_plug_mr_path;
			$file = $wp_plug_mr_path. '/mrcfg.obj';
			$upload_folder = $wp_plug_mr_path. '/uploads';
			
			$useNumbers = isset($_POST['useNumb'])?$_POST['useNumb']:false;
			$useKMLRoute = isset($_POST['useKMLRoute'])?$_POST['useKMLRoute']:false;
			
            $nombre_archivo = "";
            if(isset($_FILES['fKML'])){
                $nombre_archivo = $_FILES['fKML']['name'];
                $tipo_archivo = $_FILES['fKML']['type'];
                $tamano_archivo = $_FILES['fKML']['size'];
                $tmp_archivo = $_FILES['fKML']['tmp_name'];
                $archivador = $upload_folder . "/" . $nombre_archivo;
                if (!move_uploaded_file($tmp_archivo, $archivador)) {
                    
                }			
            }
			$cfg = array(
					'useNumbers' => $useNumbers,
					'useKMLRoute' => $useKMLRoute,
					'pathKML' => $nombre_archivo
				);
			$fp = fopen( $file ,'w');
			fwrite($fp,serialize($cfg));
			header( 'Content-Type: application/json' );
            echo json_encode( array("f"=> "$file"));
			//echo json_encode($cfg);
			die();
		}
		
		function upFile(){
			global $wp_plug_mr_path;
			$upload_folder = $wp_plug_mr_path. '/uploads';
			
			$nombre_archivo = $_FILES['fIMG']['name'];
			$tipo_archivo = $_FILES['fIMG']['type'];
			$tamano_archivo = $_FILES['fIMG']['size'];
			$tmp_archivo = $_FILES['fIMG']['tmp_name'];
			$archivador = $upload_folder . "/" . $nombre_archivo;
			if (!move_uploaded_file($tmp_archivo, $archivador)) {
			}
			header( 'Content-Type: application/json' );
			echo json_encode( array("res"=>"ok") );
			die();
		}
		
	}
}
?>
